//
//  ViewController.swift
//  DIplomaWork
//
//  Created by Aldres on 12.05.2020.
//  Copyright © 2020 Aldres. All rights reserved.
//

import UIKit
import ARKit
import CoreMotion
import FocusNode
import SmartHitTest
import MobileCoreServices

extension ARSCNView: ARSmartHitTest {}


class ViewController: UIViewController, ARSCNViewDelegate, UIGestureRecognizerDelegate, UIDocumentPickerDelegate {
    @IBOutlet weak var interfaceView: UIView!
    
    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var distanceToPointLabel: UILabel!
    let fileManager = FileManager.default

    
    let focusNode = FocusSquare()
    let pedometer = CMPedometer()
    let stepsRange = 1
    var currentSteps = 0
    var isCountingSteps = false
    
    var worldMapURL: URL = {
        do {
            return try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent("Points")
        } catch {
            fatalError(".")
        }
    }()
    
    
    @IBAction func loadWmapClick(_ sender: Any) {
        let documentPicker =
            UIDocumentPickerViewController(documentTypes: [kUTTypeFolder as String],
                                           in: .open)

        documentPicker.delegate = self

        // Set the initial directory.

        // Present the document picker.
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL])  {
        print(urls[0])
        var wmapURL = urls[0]
//        wmapURL.appendPathComponent("map")
        print(wmapURL)
        do {
            let map = try loadWorldMap(wmapURL: wmapURL)
        }
        catch {
            print(error.localizedDescription)
            return
        }
    }
    
    @IBAction func saveWmapClick(_ sender: Any) {
                sceneView.session.getCurrentWorldMap { (worldMap, error) in
                    guard let worldMap = worldMap else {
                        print("Карта еще не создана")
                        return
                    }
        
                    do {
                        try self.archive(worldMap: worldMap)
                        DispatchQueue.main.async {
                            print("Карта сохранена")
                            return
                        }
                    } catch {
                        fatalError("Ошибка при сохранении: \(error.localizedDescription)")
                    }
                }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupGestures()
        sceneView.delegate = self
        sceneView.showsStatistics = true
        self.focusNode.viewDelegate = sceneView
//        sceneView.scene.rootNode.addChildNode(self.focusNode)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal, .vertical]
        sceneView.debugOptions = [.showWorldOrigin, ARSCNDebugOptions.showFeaturePoints]
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sceneView.session.pause()
    }
    
    
    func createTargetNodeGeometry() -> SCNNode {
        let yNode = SCNNode()
        yNode.geometry = SCNSphere(radius: 0.03)
        yNode.geometry?.firstMaterial?.diffuse.contents = UIColor.red
        return yNode
    }
    
        
    func calcDistanceToNode(node: SCNNode) -> Float {
        let distance = simd_distance(node.simdTransform.columns.3, (sceneView.session.currentFrame?.camera.transform.columns.3)!);
        print(String(distance))
        return distance
    }
    
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
          let node = SCNNode()
          node.geometry = SCNSphere(radius: 0.03)
          node.geometry?.firstMaterial?.diffuse.contents = UIColor.blue
          return node
     }
    
    var hitPoints = [SCNVector3]()
    var addedNodes = [SCNNode]()
    func setupGestures() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tapGesture.delegate = self
        self.view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func handleTap(_ sender: UITapGestureRecognizer) {
        print("handled")
        guard let hitTestResult = sceneView
            .hitTest(sender.location(in: sceneView), types: [.existingPlaneUsingGeometry, .estimatedHorizontalPlane])
            .first
            else { return }
        let anchor = ARAnchor(transform: hitTestResult.worldTransform)
        sceneView.session.add(anchor: anchor)
    }
    
    func archive(worldMap: ARWorldMap) throws {
        let data = try NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true)
        let activityController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
//        let data = try NSKeyedArchiver.archivedData(withRootObject: worldMap, requiringSecureCoding: true)
//        try data.write(to: self.worldMapURL, options: [.atomic])
    }

    
    
    func loadWorldMap(wmapURL : URL)  {
        
        
        guard wmapURL.startAccessingSecurityScopedResource() else {
            // Handle the failure here.
            return
        }

        // Make sure you release the security-scoped resource when you are done.
        defer { wmapURL.stopAccessingSecurityScopedResource() }

        // Use file coordination for reading and writing any of the URL’s content.
        var error: NSError? = nil
        NSFileCoordinator().coordinate(readingItemAt: wmapURL, error: &error) { (url) in
            
            let keys : [URLResourceKey] = [.nameKey, .isDirectoryKey]
            
            // Get an enumerator for the directory's content.
            guard let fileList =
                FileManager.default.enumerator(at: url, includingPropertiesForKeys: keys) else {
                    print("*** Unable to access the contents of \(wmapURL.path) ***\n")
                    return
            }
            
            for case let file as URL in fileList {
                print(file)
                // Also start accessing the content's security-scoped URL.
                guard url.startAccessingSecurityScopedResource() else {
                    // Handle the failure here.
                    continue
                }
                        
                // Make sure you release the security-scoped resource when you are done.
                defer { url.stopAccessingSecurityScopedResource() }
                do {
                    let wmapData = try Data(contentsOf: file)
                  guard let worldMap = try NSKeyedUnarchiver.unarchivedObject(ofClass: ARWorldMap.self, from: wmapData)
                      else { throw ARError(.invalidWorldMap) }
                 resetTrackingConfiguration(with: worldMap)

                } catch {
                    print("VOT TYT")
                    print(error.localizedDescription)
                }

                // Do something with the file here.
            }



//            else { throw ARError(.invalidWorldMap) }
    }
    }
    
    @IBAction func stopRouteRecordingClick(_ sender: Any) {
        self.pedometer.stopUpdates()
        self.isCountingSteps = false
    }
    

    @IBAction func startRouteRecordingClick(_ sender: Any) {
        startCountingSteps()
    }
    
    
    func resetTrackingConfiguration(with worldMap: ARWorldMap? = nil) {
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]
        
        let options: ARSession.RunOptions = [.resetTracking, .removeExistingAnchors]
        if let worldMap = worldMap {
            configuration.initialWorldMap = worldMap
        } else {
            print("error with wmap")
        }
        
        sceneView.debugOptions = [.showFeaturePoints]
        sceneView.session.run(configuration, options: options)
    }
    
    func startCountingSteps() {
        self.isCountingSteps = true
        
        self.pedometer.startUpdates(from: Date()) {
            
            [weak self] pedometerData, error in
            
            guard let pedometerData = pedometerData, error == nil else {
                print("Error getting pedometer data")
                return
                
            }
            
            DispatchQueue.main.async {
                
                let walkedDistanceStepsData = Double(truncating: pedometerData.distance!)
                let distance = Double(round(walkedDistanceStepsData))
                print(distance)
                                
                if pedometerData.numberOfSteps.intValue >= (self?.stepsRange)! + (self?.currentSteps)! {
                    print("creating sphere")
                    self?.currentSteps = pedometerData.numberOfSteps.intValue
                    print("Current steps: \(self?.currentSteps)")
//                self?.lblDistance?.text = "\(distance) m"
                                        
                    self?.createSphereUnderFeet()
                    
                }
                
            }
            
        }
        
    }
    
    
    func createSphereUnderFeet() {

    if isCountingSteps {

        let sphere = SCNSphere(radius: 0.1)
    let sphereMaterial = SCNMaterial()
    sphereMaterial.diffuse.contents = UIColor.red
    sphere.materials = [sphereMaterial]

    let node = SCNNode(geometry: sphere)
    let testForFeaturePoints = sceneView.hitTest(self.sceneView.center, types: [ARHitTestResult.ResultType.featurePoint])
        guard let hitFeature = testForFeaturePoints.first else {
            print("Couldn't find location for sphere")
            return
            
        }
    
    let hitPosition = SCNVector3Make(hitFeature.worldTransform.columns.3.x, hitFeature.worldTransform.columns.3.y, hitFeature.worldTransform.columns.3.z)
//        node.position = self.focusNode.position
        self.sceneView.session.add(anchor: hitFeature.anchor!)
//    self.sceneView.scene.rootNode.addChildNode(node)
    } else {return}
    }
    
    
    
    //
    //    @IBAction func loadBarButtonItemDidTouch(_ sender: UIBarButtonItem) {
    //        guard let worldMapData = retrieveWorldMapData(from: worldMapURL),
    //            let worldMap = unarchive(worldMapData: worldMapData) else { return }
    //        resetTrackingConfiguration(with: worldMap)
    //    }
    
}


